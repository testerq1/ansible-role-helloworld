Ansible Role: Hello World
=========

[![Build Status](https://travis-ci.org/ravihuang/ansible-role-helloworld.svg?branch=master)](https://travis-ci.org/ravihuang/ansible-role-helloworld)

An Ansible role for automate-with-ansible book.


Requirements
------------

None.

Role Variables
--------------

See the `defaults/main.yml`.

```
world: "world"
```

Dependencies
------------

None.

Example Playbook
----------------

Including an example of how to use your role (for instance, with variables passed in as parameters) is always nice for users too:

    - hosts: servers
      roles:
         - { role: ansible-role-helloworld }

Execute result:

	$ ansible-playbook setup.yml
	
	PLAY [Play 'Hello World'] ******************************************************
	
	TASK [setup] *******************************************************************
	ok: [localhost]
	
	TASK [echo 'hello world'] ******************************************************
	changed: [localhost]
	
	TASK [print message] ***********************************************************
	ok: [localhost] => {
	    "msg": [
	        "Hello world"
	    ]
	}
	
	PLAY RECAP *********************************************************************
	localhost                  : ok=3    changed=1    unreachable=0    failed=0


License
-------

Copyright (c) testerq from 2018-2018 under the MIT license.

